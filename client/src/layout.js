import React, { Component } from 'react';
import TopNav from './components/TopNavbar';

class Layout extends Component {
    render() {
        return (
            <div style={{ backgroundColor: '#f8f9fa' }}>
                <TopNav />
                <div style={{ paddingTop: '40px' }}>
                    {this.props.children}
                </div>

            </div>
        );
    }
}

export default Layout;