import React, { Component } from 'react';
import Layout from '../../layout';
import { connect } from 'react-redux'
import { getresponse, getresponseDetail } from './../../redux/actions/response'
import { withRouter } from 'react-router-dom';
import { Chart } from "react-google-charts";

class Response extends Component {
    constructor(props) {
        super(props)
        this.state = {
            category: '',
            show: false,
            question: '',
            data: [],
            graphData: [

            ]
        }
    }

    open = (d) => {
        this.setState({
            show: true,
            question: d._id
        })
    }
    componentDidMount = async () => {
        const { state } = this.props.location
        await this.props.getresponse(`${state.id}`)
        let data = []
        let GraphD = [
            ['Question', 'Option A', 'Option B'],

        ]
        this.setState({ category: state.id })
        for (let i = 0; i < this.props.responses.length; i++) {
            const resp = await this.props.getresponseDetail(`${this.props.responses[i].question._id}`)
            data.push({
                body: this.props.responses[i].question.body,
                OptionACount: resp.data.count1,
                OptionbCount: resp.data.count2,
                sum: resp.data.count1 + resp.data.count2
            })
            GraphD.push([this.props.responses[i].question.body, resp.data.count1, resp.data.count2])

        }
        this.setState({ data: data, graphData: GraphD })

    }
    render() {

        return (
            <Layout>
                <div className="container">
                    <h3 style={{ textAlign: 'center' }}>Response Outcome</h3>
                    {this.state.graphData.length === 0 ? <div>Loading Data</div> : <Chart
                        width={'100%'}
                        height={'300px'}
                        chartType="BarChart"
                        loader={<div>Loading Chart</div>}
                        data={this.state.graphData}
                        options={{
                            title: 'OutCome from the survay ',
                            chartArea: { width: '60%' },
                            colors: ['#0f2a5f', '#87CEEB'],
                            hAxis: {
                                title: 'Total Count',
                                minValue: 0,
                            },
                            vAxis: {
                                title: 'Question',
                            },
                        }}
                        // For tests
                        rootProps={{ 'data-testid': '1' }}
                    />}
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Question</th>
                                <th scope="col">Total participants</th>
                                <th scope="col">Option 1 </th>
                                <th scope="col">Option 2 </th>


                            </tr>
                        </thead>
                        <tbody>
                            {this.state.data.map((resp, i) => (
                                <tr key={i}>
                                    <th scope="row">{i + 1}</th>
                                    <td>{resp.body}</td>
                                    <td>{resp.sum}</td>
                                    <td>{resp.OptionACount}</td>
                                    <td>{resp.OptionbCount}</td>

                                </tr>
                            ))}

                        </tbody>
                    </table>


                </div>
            </Layout>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        error: state.responseData.error,
        loading: state.responseData.loading,
        responses: state.responseData.responses,
    }

};

export default connect(mapStateToProps, { getresponse, getresponseDetail })(withRouter(Response));

