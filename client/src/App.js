import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Surveys from './pages/surveys'
import Home from "./pages/home";
import Survey from "./pages/surveys/categories";
import Category from "./pages/admin";
import { Provider } from "react-redux";
import store from "./redux/store/store";
import Questions from "./pages/admin/questions";
import Response from "./pages/admin/response";
import ResponseDetail from "./components/responseDetail";
function App() {
  const user = JSON.parse(localStorage.getItem('userInfo'));



  return (
    <Provider store={store}>
      <Router>
        <div>

          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/our-surveys" component={Survey} />
            <Route exact path="/our-surveys/:category" component={Surveys} />
            <Route path="/admin/survey/categories" exact >{user && !user.admin ? <Redirect to="/" /> : <Category />}</Route>
            <Route path="/admin/survey/categories/:slug/questions" exact >{user && !user.admin ? <Redirect to="/" /> : <Questions />}</Route>
            <Route path="/admin/survey/categories/:slug/response" exact >{user && !user.admin ? <Redirect to="/" /> : <Response />}</Route>
            <Route path="/admin/survey/categories/:slug/response/responsedetail" exact >{user && !user.admin ? <Redirect to="/" /> : <ResponseDetail />}</Route>

          </Switch>
        </div>

      </Router>

    </Provider>
  );
}

export default App;
